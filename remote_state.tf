data "google_compute_network" "my_network" {
  name    = var.network
  project = var.project_id
}

data "google_compute_subnetwork" "my_subnetwork" {
  name    = var.subnet
  region  = var.region
  project = var.project_id
}

