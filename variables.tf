variable "name" {
  type        = string
  description = "Name of the Notebook"
}

variable "project_id" {
  type        = string
  description = "google cloud project ID"
  default     = null
}

variable "region" {
  type        = string
  description = "region where you want notebook to be created"
  #default     = "us-central1"
}

variable "location" {
  type        = string
  description = "location of Notebook creation"
  #default     = "us-central1-b"
}

variable "credentials" {
  type        = string
  description = "service account keys"
  default     = null

}

variable "machine_type" {
  type        = string
  description = "hardware details of the notebook instance"
  #default     = "e2-highmem-2"
}

variable "network" {
  type        = string
  description = "name of the network the notebook instance will be on"
  #default     = "default"
}

variable "subnet" {
  type        = string
  description = "name of the subnetwork the notebook instance will be on"
  #default     = "default"
}


variable "service_account" {
  type        = string
  description = "service account used to create the notebook instance"
  default     = null

}

variable "boot_disk_type" {
  type        = string
  description = "type of storage disk used"
  #default     = "PD_SSD"
}

variable "boot_disk_size_gb" {
  type        = number
  description = "size of the hard drive"
  #default     = 100
}

variable "image_name" {
  type        = string
  description = "VM image name"
  #default     = "common-cpu-notebooks-v20221107"
}

variable "idle-timeout-seconds" {
  type        = number
  description = "Allowed time notebook instance can be idle"
  default     = 7200
}

variable "enable-guest-attribute" {
  type        = bool
  description = ""
  default     = "true"
}

variable "proxy-mode" {
  type        = string
  description = ""
  default     = "service_account"
}

variable "report-system-health" {
  type        = bool
  description = ""
  default     = "true"
}

variable "enable-terminal" {
  type        = bool
  description = ""
  default     = "true"
}

variable "notebook-disable-downloads" {
  type        = bool
  description = ""
  default     = "false"
}

variable "disable-nbconvert" {
  type        = bool
  description = ""
  default     = "true"
}

variable "notebook-disable-root" {
  type        = bool
  description = ""
  default     = "true"
}

variable "metadata_version" {
  type        = number
  description = ""
  default     = 100
}
