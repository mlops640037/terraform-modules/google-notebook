output "instance_id" {
  value = google_notebooks_instance.notebook_instance_one.id
}

output "instance_state" {
  value = google_notebooks_instance.notebook_instance_one.state
}

output "instance_create_time" {
  value = google_notebooks_instance.notebook_instance_one.create_time
}

output "instance_update_time" {
  value = google_notebooks_instance.notebook_instance_one.update_time
}

output "instance_effective_labels" {
  value = google_notebooks_instance.notebook_instance_one.effective_labels
}