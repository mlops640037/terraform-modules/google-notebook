This is a guide on the overview of the vertexAI notebook module
and how the best way to use it. 

OVERVIEW: 
The notebook module consists of the following components:
 
a. main.tf --> This contains the google notebooks resource block and its arguments 

b. variables.tf --> This contains the default values of the arguments declared in main.tf. The default values will be used 

c. remote_state.tf ---> This contains the 2 data blocks here, network(VPC) and subnetwork, with the value for both of them set to "default" 

d. output.tf ---> This contain the output variables from running the notebook resource block. The output variables here capture the state of the notebook instance after the "terraform apply" command has been executed.  


How To Use the notebook terraform module: 

a. via a Module Block:
   
   The benefit of using the module block is that in a single module block, 
you can create multiple notebooks and group notebooks creation based on the the purposed they were created for. Thus making it easy to keep track of them for each google cloud project.  

So we create an instance of the notebook resource via the module block, with the "source" variable pointing to the path of the notebook module and all the other variables needed to instantiate it.

b. variable.tf file: This file contains the declaration of variables stated in the module block
along with the default value (default values do not have to be set). 

c. terraform.tfvars: This is file you add the value of the variables called in the module block you wish to use. if you do not define it, then the default value in the variables.tf file will be used.  


Check the examples directory for more information on how the notebook module is to be leveraged. 


NOTE:

a. Make sure the "source" variable is pointing to the directory/path of the notebook module 

b. In the module block, if you do not specify the value of an argument that has been defined in the google notebook instance resource block, the default value in the variables.tf file (the one in the same directory as main.tf) will be used. 


Keeping Track of the Terraform State of Each Project: 

It is important to have a separate terraform state file for each Google cloud project you are working on. The best practice in other to achieve this is to: 

a. Create a sub directory for each google cloud project environment you are creating infrastructure for within your branch. Each subdirectory will have a module.tf, variables.tf and terraform.tfvars

b. Create a remote state location to keep each terraform state file for every GCP you manage with terraform. By doing this, terraform will have a state file for each google cloud project 

    