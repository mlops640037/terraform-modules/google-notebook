#Note: add "editor" or "Notebooks Admin" roles to service account to address the 
# notebooks.operations.get permisson error. 
# source: https://gcp.permissions.cloud/iam/notebooks

locals {
  terraform = "true"
  notebook  = "user-managed"
  project   = "deeplearning-platform-release"
}

resource "google_notebooks_instance" "notebook_instance_one" {

  name         = var.name
  project      = var.project_id
  location     = var.location
  machine_type = var.machine_type

  service_account = var.service_account

  boot_disk_type    = var.boot_disk_type
  boot_disk_size_gb = var.boot_disk_size_gb
  no_public_ip      = true
  network           = data.google_compute_network.my_network.id
  subnet            = data.google_compute_subnetwork.my_subnetwork.id

  labels = {
    terraform = local.terraform
    notebook  = local.notebook
  }

  vm_image {
    project    = local.project
    image_name = var.image_name # pytorch-1-13-cu113-notebooks-v20230925-debian-11-py310
  }

  metadata = {
    idle-timeout-seconds       = var.idle-timeout-seconds
    enable-guest-attribute     = var.enable-guest-attribute
    proxy-mode                 = var.proxy-mode
    report-system-health       = var.report-system-health
    enable-terminal            = var.enable-terminal
    notebook-disable-downloads = var.notebook-disable-downloads
    disable-nbconvert          = var.disable-nbconvert
    notebook-disable-root      = var.notebook-disable-root
    version                    = var.metadata_version
  }

}

